# Purescript-datetime-parser

An ISO8601 parser for `Date` and `DateTime`.

## Usage
`DateTime` parse and unparse:
```purescript
dateTime = parseLocalDateTime "2017-05-07T16:44:15.234-03:20"

dateTimeToString (Left _) = ""
dateTimeToString (Right dt) = unparseLocalDateTime dt

dateTimeString = dateTimeToString dateTime
```

`Date` parse and unparse:
```purescript
date = parseDate "2017-05-07"

dateToString (Left _) = ""
dateToString (Right dt) = unparseDate dt

dateString = dateToString date
```

## Build
```
npm i
npm run bower
npm run build
```

## Test
```
npm test
```