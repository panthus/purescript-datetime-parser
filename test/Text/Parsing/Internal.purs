module Test.Text.Parsing.Internal where

import Prelude
import Test.Unit.Assert as Assert
import Data.DateTime (Minute)
import Data.Either (Either(..), either)
import Data.Enum (toEnum)
import Data.Maybe (maybe)
import Test.Unit (TestSuite, suite, test)
import Text.Parsing.Internal (digit, digitN, enum, leftPad, toString, toStringPad)
import Text.Parsing.Parser (parseErrorMessage, runParser)

internalSuite :: forall e. TestSuite e
internalSuite =
  suite "Text.Parsing.Internal" do
    test "enum should convert an Int into a BoundedEnum" do
      let actual = either (parseErrorMessage >>> Left) Right (runParser "" $ enum "error" 20) :: Either String Minute
      let expected = (maybe (Left "") Right $ toEnum 20) :: Either String Minute
      Assert.equal expected actual
    test "enum should return custom error message on fail" do
      let actual = either (parseErrorMessage >>> Left) Right (runParser "" $ enum "error" (-1)) :: Either String Minute
      let expected = (Left "Expected error, actual is -1.")
      Assert.equal expected actual
    test "digit should convert a Char into an Int" do
      let actual = either (parseErrorMessage >>> Left) Right (runParser "20" digit)
      let expected = Right 2
      Assert.equal expected actual
    test "digit should fail on non digit characters" do
      let actual = either (parseErrorMessage >>> Left) Right (runParser "b" $ digit)
      let expected = Left "Expected a digit between 0 and 9, actual is 'b'."
      Assert.equal expected actual
    test "digitN should convert a String into an Int" do
      let actual = either (parseErrorMessage >>> Left) Right (runParser "200" $ digitN 2)
      let expected = Right 20
      Assert.equal expected actual
    test "digitN should fail on non digit characters" do
      let actual = either (parseErrorMessage >>> Left) Right (runParser "2b" $ digitN 2)
      let expected = Left "Expected a digit between 0 and 9, actual is 'b'."
      Assert.equal expected actual
    test "toString should convert an Int into a String" do
      let actual = toString 123
      let expected = "123"
      Assert.equal expected actual
    test "leftPad should add the specified Char until the given length is reached" do
      let actual = leftPad 3 '0' "12"
      let expected = "012"
      Assert.equal expected actual
    test "leftPad should do nothing if specified length is smaller than given String" do
      let actual = leftPad 0 '0' "12"
      let expected = "12"
      Assert.equal expected actual
    test "toStringPad should add '0' until the given length is reached" do
      let actual = toStringPad 3 12
      let expected = "012"
      Assert.equal expected actual
    test "toStringPad should do nothing if specified length is smaller than given String" do
      let actual = toStringPad 0 12
      let expected = "12"
      Assert.equal expected actual