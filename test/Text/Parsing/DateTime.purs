module Test.Text.Parsing.DateTime where

import Prelude
import Test.Unit.Assert as Assert
import Data.Either (Either(Right, Left))
import Test.Unit (Test, TestSuite, failure, success, suite, test)
import Text.Parsing.DateTime (parseDate, parseLocalDateTime, unparseDate, unparseLocalDateTime)

testDateParse :: forall e. String -> Test e
testDateParse date =
  testDateParse' date date

testDateParse' :: forall e. String -> String -> Test e
testDateParse' expectedDate date =
  case parseDate date of
    Left e -> failure e
    Right d -> Assert.equal expectedDate $ unparseDate d

testDateTimeParse :: forall e. String -> Test e
testDateTimeParse dateTime =
  testDateTimeParse' dateTime dateTime

testDateTimeParse' :: forall e. String -> String -> Test e
testDateTimeParse' expectedDateTime dateTime =
  case parseLocalDateTime dateTime of
    Left e -> failure e
    Right d -> Assert.equal expectedDateTime $ unparseLocalDateTime d

failDateTimeParse :: forall e. String -> Test e
failDateTimeParse dateTime =
  case parseLocalDateTime dateTime of
    Left e -> success
    Right d -> failure $ "dateTime " <> dateTime <> " should not be parsable"

dateTimeSuite :: forall e. TestSuite e
dateTimeSuite =
  suite "Text.Parsing.DateTime" do
    test "parseDate followed by unparseDate should result in the same String but then formatted" do
      testDateParse "2017-05-07" -- Formatted
      testDateParse' "2017-05-07" "20170507" -- Unformatted
    test "parseLocalDateTime followed by unparseLocalDateTime should result in the same String but then formatted" do
      testDateTimeParse' "2017-05-07T22:04:15+02" "2017-05-07T22:04:15+02:00" -- Normal UTC+2
      testDateTimeParse' "2017-05-07T20:04:15+00" "2017-05-07T20:04:15+00:00" -- UTC
      testDateTimeParse' "2017-05-07T20:04:15+00" "2017-05-07T20:04:15Z" -- UTC special syntax
      testDateTimeParse "2017-05-07T20:04:15-06" -- UTC-6 without minutes
      testDateTimeParse' "2017-05-07T17:04:15-03" "2017-05-07T17:04:15-03:00" -- Normal UTC-3
      testDateTimeParse "2017-05-07T16:44:15-03:20" -- Normal UTC-3.20
      testDateTimeParse' "2017-05-07T16:44:15-03:20" "20170507T164415-0320" -- Not formatted
      testDateTimeParse "2017-05-07T16:44:15.234-03:20" -- With milleseconds
      testDateTimeParse "2017-05-07T16:44-03:20" -- Without seconds
      testDateTimeParse "2017-05-07T16-03:20" -- Without seconds and minutes
    test "parseLocalDateTime should error on invalid input" do
      failDateTimeParse "2017-05-07T16:44.234-03:20" -- Milliseconds without seconds
      failDateTimeParse "2017-05-07T16:44" -- No timezone