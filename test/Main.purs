module Test.Main where

import Prelude
import Control.Monad.Aff.AVar (AVAR)
import Control.Monad.Aff.Console (CONSOLE)
import Control.Monad.Eff (Eff)
import Test.Text.Parsing.DateTime (dateTimeSuite)
import Test.Text.Parsing.Internal (internalSuite)
import Test.Unit.Console (TESTOUTPUT)
import Test.Unit.Main (runTest)

main :: Eff (console :: CONSOLE, testOutput :: TESTOUTPUT, avar :: AVAR) Unit
main = runTest do
  internalSuite
  dateTimeSuite