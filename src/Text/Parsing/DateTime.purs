module Text.Parsing.DateTime (parseLocalDateTime, unparseLocalDateTime, parseDate, unparseDate) where

import Prelude
import Control.Alt ((<|>))
import Data.Date (Date, canonicalDate, day, month, year)
import Data.DateTime (DateTime(..), Time(..))
import Data.DateTime.Locale (LocalDateTime, LocalValue(..), Locale(..))
import Data.Either (Either(..), either)
import Data.Enum (class BoundedEnum, fromEnum)
import Data.Int (floor, toNumber)
import Data.Maybe (Maybe(Nothing))
import Data.Time (setMillisecond, setMinute, setSecond)
import Data.Time.Duration (Minutes(..))
import Math (abs, (%))
import Text.Parsing.Internal (digitN, enum, toString, toStringPad)
import Text.Parsing.Parser (ParseError, ParserT, parseErrorMessage, parseErrorPosition, runParser)
import Text.Parsing.Parser.Combinators (try)
import Text.Parsing.Parser.Pos (Position(..))
import Text.Parsing.Parser.String (class StringLike, char, eof)

-- | Parse a local date and time in ISO 8601 format.
-- | Note that it expects a year after 1900.
parseLocalDateTime :: String -> Either String LocalDateTime
parseLocalDateTime input =
  either (toError >>> Left) Right $ runParser input (localDateTime <* eof)

-- | Parse a date in ISO 8601 format.
-- | Note that it expects a year after 1900.
parseDate :: String -> Either String Date
parseDate input =
  either (toError >>> Left) Right $ runParser input (date <* eof)

-- | Unparse a local date time into a ISO 8601 string.
unparseLocalDateTime :: LocalDateTime -> String
unparseLocalDateTime (LocalValue (Locale _ (Minutes o)) (DateTime d (Time h m s ss))) =
  unparseDate d 
    <> "T" <> toStringPad 2 (fromEnum h)
    <> optional (toStringPad 2) ":" (fromEnum m)
    <> optional (toStringPad 2) ":" (fromEnum s) 
    <> optional (toStringPad 3) "." (fromEnum ss)
    <> offset o
    where
      optional :: (Int -> String) -> String -> Int -> String
      optional _ _ 0 = ""
      optional t c v = c <> t v

      offset :: Number -> String
      offset os = (if os >= 0.0 then "+" else "-")
        <> toStringPad 2 (floor (abs os / 60.0))
        <> optional (toStringPad 2) ":" (floor (abs os % 60.0))

-- | Unparse a date into a ISO 8601 string.
unparseDate :: Date -> String
unparseDate d =
  ys <> "-" <> ms <> "-" <> ds
    where
      ys = toStringPad 4 $ fromEnum $ year d
      ms = toStringPad 2 $ fromEnum $ month d
      ds = toStringPad 2 $ fromEnum $ day d

toError :: ParseError → String
toError error =
  parseErrorMessage error <> (position $ parseErrorPosition error)
  where
    position (Position { line: line, column: column }) = " at line " <> toString line <> ", column " <> toString column

localDateTime :: forall m s. Functor m => StringLike s => Monad m => ParserT s m LocalDateTime
localDateTime =
  flip LocalValue <$> dateTime <*> locale

dateTime :: forall s m. StringLike s => Monad m => ParserT s m DateTime
dateTime = DateTime <$> date <*> (char 'T' *> time)

date :: forall s m. StringLike s => Monad m => ParserT s m Date
date = canonicalDate
  <$> (enum "year" =<< digitN 4)
  <*> (enum "month" =<< (char '-' *> digitN 2) <|> digitN 2)
  <*> (enum "day" =<< (char '-' *> digitN 2) <|> digitN 2)

hh :: forall s m. StringLike s => Monad m => ParserT s m Time
hh = Time <$> (enum "hour" =<< digitN 2) <*> pure bottom <*> pure bottom <*> pure bottom

hhmm :: forall s m. StringLike s => Monad m => ParserT s m Time
hhmm = try $ flip setMinute <$> hh <*> (enum "minute" =<< (char ':' *> digitN 2) <|> digitN 2)

hhmmss :: forall s m. StringLike s => Monad m => ParserT s m Time
hhmmss = try $ flip setSecond <$> hhmm <*> (enum "second" =<< (char ':' *> digitN 2) <|> digitN 2)

hhmmsssss :: forall s m. StringLike s => Monad m => ParserT s m Time
hhmmsssss = try $ flip setMillisecond <$> hhmmss <*> (enum "millisecond" =<< (char '.' *> digitN 3))

time :: forall s m. StringLike s => Monad m => ParserT s m Time
time = hhmmsssss <|> hhmmss <|> hhmm <|> hh

locale :: forall s m. StringLike s => Monad m => ParserT s m Locale
locale = Locale Nothing <$> (utc <|> nOffset <|> pOffset)
  where
    utc :: ParserT s m Minutes
    utc = char 'Z' *> pure (Minutes 0.0)

    offset :: ParserT s m Minutes
    offset = timeToMinutes <$> (hhmm <|> hh)

    nOffset :: ParserT s m Minutes
    nOffset = negate <$> (char '-' *> offset)

    pOffset :: ParserT s m Minutes
    pOffset = char '+' *> offset

timeToMinutes :: Time -> Minutes
timeToMinutes (Time h m s ss) =
  Minutes $ (fromEnumN h * 60.0) + fromEnumN m + (fromEnumN s / 60.0) + (fromEnumN ss / 60000.0)
  where
    fromEnumN :: forall a. BoundedEnum a ⇒ a → Number
    fromEnumN = fromEnum >>> toNumber
