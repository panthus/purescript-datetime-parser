module Text.Parsing.Internal where

import Prelude
import Data.Char (toCharCode)
import Data.Enum (class BoundedEnum, toEnum)
import Data.Int (decimal, toStringAs)
import Data.Maybe (maybe)
import Data.String (length, singleton)
import Text.Parsing.Parser (ParserT, fail)
import Text.Parsing.Parser.Combinators (try)
import Text.Parsing.Parser.String (class StringLike, anyChar)

-- | Parse an `Int` as a `BoundedEnum`.
enum :: forall s m a. StringLike s => Functor m => Monad m => BoundedEnum a => String -> Int -> ParserT s m a
enum error value =
    maybe (fail $ "Expected " <> error <> ", actual is " <> show value <> ".") pure $ toEnum value

-- | Parse an `Int` of the given length.
-- | For example `digitN 3` for string "123" would become int 123.
digitN :: forall s m. StringLike s => Functor m => Monad m => Int -> ParserT s m Int
digitN amount
    | amount == 0 = pure 0
    | otherwise = try do
      x1 <- digitN (amount - 1)
      x2 <- digit
      pure $ x1 * 10 + x2

-- | Parse a single character as a digit.
digit :: forall s m. StringLike s => Functor m => Monad m => ParserT s m Int
digit = try do
    char <- anyChar
    let d = toCharCode char - toCharCode '0'
    if d < 0 || d > 9
      then fail $ "Expected a digit between 0 and 9, actual is " <> show char <> "."
      else pure d

-- | Convert an `Int` to a decimal string.
toString :: Int -> String
toString = toStringAs decimal

-- | Pad the given string with the char until the specified length is reached.
leftPad :: Int -> Char -> String -> String
leftPad d _ s | d <= 0 || d <= length s = s
leftPad d c s = leftPad (d - 1) c $ singleton c <> s

-- | Convert an `Int` to a decimal string and pad it with '0' to the specified length.
toStringPad :: Int -> Int -> String
toStringPad pad = toString >>> leftPad pad '0'
